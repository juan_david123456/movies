from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Pelicula(models.Model):
    titulo = models.CharField(max_length=200)
    sinopsis = models.CharField(max_length=2000)
    anio = models.IntegerField()
    actores = models.CharField(max_length=2000)
    duracion = models.IntegerField()
    enCartelera = models.IntegerField()
    img_url = models.CharField(max_length = 500)
    class Meta:
        app_label: 'pelicula'

class Categoria(models.Model):
    nombre = models.CharField(max_length=45)

    class Meta:
        app_label:'categoria'

class Calificacion(models.Model):
    valor = models.IntegerField()
    usuario = models.ForeignKey(
        User,
        related_name='calificacion_usuario',
        on_delete=models.CASCADE
    )
    pelicula = models.ForeignKey(
        Pelicula,
        related_name='calificacion_pelicula',
        on_delete=models.CASCADE
    )

    class Meta:
        app_label:'categoria'

class Comentario(models.Model):
    texto = models.CharField(max_length=2000)
    fechaHora = models.DateTimeField(auto_now_add=True)
    activo = models.IntegerField()
    usuario = models.ForeignKey(
        User,
        related_name='comentario_usuario',
        on_delete=models.CASCADE
    )
    pelicula = models.ForeignKey(
        Pelicula,
        related_name='comentario_pelicula',
        on_delete=models.CASCADE
    )

    class Meta:
        app_label:'comentario'

class PeliculaCategoria(models.Model):
    pelicula = models.ForeignKey(
        Pelicula,
        related_name='peliculacategoria_pelicula',
        on_delete=models.CASCADE
    )
    categoria = models.ForeignKey(
        Categoria,
        related_name='peliculacategoria_categoria',
        on_delete=models.CASCADE
    )

    class Meta:
        app_label:'comentario'
