from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import *
from django.core.files.storage import default_storage

# Plantillas


def signin(request):
    categories = Categoria.objects.all()
    contexto = {'usuario': request.user, 'categorias': categories}
    return render(request, 'app/signin.html', contexto)


def signup(request):
    categories = Categoria.objects.all()
    contexto = contexto = {'usuario': request.user, 'categorias': categories}
    return render(request, 'app/signup.html', contexto)


def index(request):
    return render(request, 'app/details.html')
    # return HttpResponse("Hola, mundo")


def home(request):
    print(request.user.is_anonymous)
    categories = Categoria.objects.all()
    movies = Pelicula.objects.all()
    cartelera = list()
    ranking = list()
    last = list()
    rank = list()
    for m in movies:
        rat = Calificacion.objects.filter(pelicula_id=m.id)
        cont = 0
        if rat:
            for r in rat:
                cont += r.valor
            ranking.append([m, cont/len(rat)])
        else:
            ranking.append([m, 0])
    ranking.sort(key=lambda tup: tup[1])
    cont = 0
    for k in ranking:
        k[0].v = int(k[1])
        if cont <= 9:
            rank.append(k[0])
        if k[0].anio > 2015:
            last.append(k[0])
        if k[0].enCartelera:
            cartelera.append(k[0])

    context = {
        'usuario': request.user,
        'categorias': categories,
        'cartelera': cartelera,
        'rank': rank,
        'nuevas': last
    }
    return render(request, 'app/home.html', context)

def detalle(request,p_id):
    categories = Categoria.objects.all()
    movie = Pelicula.objects.get(id=p_id)
    comments_s = Comentario.objects.filter(pelicula_id = p_id)
    comments = list()
    for c in comments_s:
        if c.activo:
            comments.append(c)
    context = {
        'pelicula':movie,
        'usuario': request.user,
        'categorias': categories,
        'comentarios':comments
    }
    return render(request, 'app/details.html', context)

def catalogo(request, c_id):
    categories = Categoria.objects.all()
    categoria = Categoria.objects.get(id=c_id)
    pelicula_categoria = PeliculaCategoria.objects.filter(categoria_id=c_id)
    peliculas = list()
    for p in pelicula_categoria:
        peliculas.append(p.pelicula)
    contexto = {'categorias': categories,
                'fuente': categoria.nombre, 'peliculas': peliculas}
    return render(request, 'app/catalogo.html', contexto)




def lista_categorias(request):
    categorias = Categoria.objects.all()
    contexto = {'categorias': categorias}
    return render(request, 'app/lista_categorias.html', contexto)

def editar_pelicula(request):
    categorias = Categoria.objects.all()
    peliculas = Pelicula.objects.all()
    contexto = {'categorias': categorias,'peliculas':peliculas}
    return render(request, 'app/admin_editar_pelicula.html', contexto)


def lista_peliculas(request):
    categorias = Categoria.objects.all()
    peliculas = Pelicula.objects.all()
    contexto = {'categorias': categorias,
                'peliculas': peliculas, 'fuente': 'Lista de Peliculas'}
    return render(request, 'app/catalogo.html', contexto)


def admin_account(request):
    categorias = Categoria.objects.all()
    contexto = {'categorias': categorias}
    return render(request, 'app/admin_account.html', contexto)


def admin_crear_categoria(request):
    return render(request, 'app/admin_crear_categoria.html')


def admin_crear_administrador(request):
    return render(request, 'app/admin_crear_administrador.html')


def admin_crear_pelicula(request):
    categorias = Categoria.objects.all()
    contexto = {'categorias': categorias}
    return render(request, 'app/admin_crear_pelicula.html', contexto)
# Operaciones


def auth_login(request):
    usuario = request.POST['username']
    clave = request.POST['contrasenia']
    u = authenticate(username=usuario, password=clave)
    if u:
        login(request, u)
    return HttpResponseRedirect(reverse('home'))


@login_required(redirect_field_name='home', login_url='sign_in')
def auth_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))


def guardar_registro(request):
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']
    user = User(email=email, username=username)
    user.set_password(password)
    user.save()
    return HttpResponseRedirect(reverse('home'))
###


def post_crear_categoria(request):
    nombre_categoria = request.POST['nombre']
    categoria = Categoria(nombre=nombre_categoria)
    categoria.save()
    return HttpResponseRedirect(reverse('home'))


def post_crear_administrador(request):
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']
    u = User()
    u.first_name = first_name
    u.last_name = last_name
    u.username = username
    u.email = email
    u.set_password(password)
    u.is_superuser = True
    u.save()
    return HttpResponseRedirect(reverse('home'))


def post_crear_pelicula(request):
    title = request.POST['nombre']
    sinopsis = request.POST['sinopsis']
    actors = request.POST['reparto']
    cartelera = request.POST['cartelera']
    m_categories = request.POST.getlist('m_categories')
    year = request.POST['anio']
    duration = request.POST['duracion']
    for f in request.FILES:
        img = request.FILES[f]
        img_name = default_storage.save(img.name, img)
        img_url = 'images/'+img.name
    p = Pelicula()
    p.titulo = title
    p.sinopsis = sinopsis
    p.actores = actors
    p.enCartelera = cartelera == "1"
    p.img_url = img_url
    p.anio = year
    p.duracion = duration
    p.save()
    for i in m_categories:
        pc = PeliculaCategoria()
        pc.pelicula = p
        pc.categoria = Categoria.objects.get(id=i)
        pc.save()
    return HttpResponseRedirect(reverse('home'))

def post_editar_pelicula(request):
    pelicula_id = request.POST['pelicula']
    pelicula = Pelicula.objects.get(id = int(pelicula_id))
    categorias = Categoria.objects.all()
    contexto = {'categorias': categorias,'pelicula':pelicula}
    return render(request, 'app/admin_crear_pelicula.html', contexto)

def post_buscar_pelicula(request):
    name = request.POST['search']
    movies = Pelicula.objects.all()
    matches = list()
    name = name.upper()
    for m in movies:
        m_title = m.titulo
        if name in m_title.upper():
            matches.append(m)
    context = {
        'usuario': request.user,
        'source': request.POST['search'],
        'peliculas': matches,
        'categorias': Categoria.objects.all(),
    }
    return render(request, 'app/catalogo.html', context)

def post_guardar_comentario(request,p_id):
    mensaje = request.POST["message"]
    rate = int(request.POST["rate"])
    p = Pelicula.objects.get(id = p_id)

    calificacion = Calificacion()
    calificacion.usuario = request.user
    calificacion.pelicula = p
    calificacion.valor = rate
    calificacion.save()

    comentario = Comentario()
    comentario.usuario = request.user
    comentario.pelicula = p
    comentario.texto = mensaje
    comentario.activo = True
    comentario.save()

    return HttpResponseRedirect(reverse('detalle', kwargs={'p_id': p_id}))

def desactivar_comentario(request,c_id):
    comment = Comentario.objects.get(id = c_id)
    p = comment.pelicula
    comment.activo = False
    comment.save()
    return HttpResponseRedirect(reverse('detalle', kwargs={'p_id': p.id}))

def post_guardar_editar_pelicula(request):
    p_id = int(request.POST['pelicula'])
    title = request.POST['nombre']
    sinopsis = request.POST['sinopsis']
    actors = request.POST['reparto']
    cartelera = request.POST['cartelera']
    m_categories = request.POST.getlist('m_categories')
    year = request.POST['anio']
    duration = request.POST['duracion']

    p = Pelicula.objects.get(id = p_id)
    p.titulo = title
    p.sinopsis = sinopsis
    p.actores = actors
    p.enCartelera = cartelera == "1"
    p.anio = year
    p.duracion = duration
    p.save()
    for i in m_categories:
        pc = PeliculaCategoria()
        pc.pelicula = p
        pc.categoria = Categoria.objects.get(id=i)
        pc.save()
    return HttpResponseRedirect(reverse('home'))

