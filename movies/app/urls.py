from django.urls import path,include
from . import views 

urlpatterns = [
    path('', views.index, name='index'), 
    path('home/',views.home, name ='home'),
    path('categories/<int:c_id>/',views.catalogo, name ='catalogo'),
    path('signin/',views.signin, name ='sign_in'),
    path('auth_login/',views.auth_login, name ='auth_login'),
    path('signup/',views.signup, name ='sign_up'),
    path('logout/',views.auth_logout, name ='log_out'),
    path('guardar_registro/',views.guardar_registro, name ='guardar_registro'),
    path('detalle/<int:p_id>/',views.detalle, name ='detalle'),
    path('admin_account/',views.admin_account, name ='admin_account'),
    path('admin_account/crear_categoria/',views.admin_crear_categoria, name ='admin_crear_categoria'),
    path('admin_account/crear_administrador/',views.admin_crear_administrador, name ='admin_crear_administrador'),
    path('admin_account/crear_pelicula/',views.admin_crear_pelicula, name='admin_crear_pelicula'),
    path('admin_account/crear_categoria_post/',views.post_crear_categoria, name='post_crear_categoria'),
    path('admin_account/crear_pelicula_post/',views.post_crear_pelicula, name='post_crear_pelicula'),
    path('admin_account/crear_administrador_post/',views.post_crear_administrador, name='post_crear_administrador'),
    path('post_buscar_pelicula/',views.post_buscar_pelicula, name='post_buscar_pelicula'),
    path('post_guardar_comentario/<int:p_id>/',views.post_guardar_comentario, name='post_guardar_comentario'),
    path('admin_account/lista_categorias/',views.lista_categorias, name ='lista_categorias'),
    path('admin_account/desactivar_comentario/<int:c_id>/',views.desactivar_comentario, name ='desactivar_comentario'),
    path('admin_account/lista_peliculas/',views.lista_peliculas, name ='lista_peliculas'),
    path('admin_account/editar_pelicula/',views.editar_pelicula, name ='editar_pelicula'),
    path('admin_account/post_editar_pelicula/',views.post_editar_pelicula, name ='post_editar_pelicula'),
    path('admin_account/post_guardar_editar_pelicula/',views.post_guardar_editar_pelicula, name ='post_guardar_editar_pelicula'),
    
]