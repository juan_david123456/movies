correos:

neiderhernandez@usantotomas.edu.co
yeisonleiva@usantotomas.edu.co
juanrodriguezt@usantotomas.edu.co
Lista casos de uso:

- Ultimas peliculas app/home/
- peliculas en catelera app/home/
- ver pelicula app/detalle/
- login /app/signin/
- register /app/signup/
- logout /app/home/
- lista de peliculas app/admin_account/lista_peliculas/
- lista de categorias app/admin_account/lista_categorias/
- crear administrador app/admin_account/crear_administrador/
- crear pelicula app/admin_account/crear_pelicula/
- crear categoria admin_account/crear_categoria/
- buscar pelicula app/app/home/
- mejor puntuacion app/home/
- peliculas por categoria app/home/
- editar pelicula app/admin_account/editar_pelicula/
- comentar pelicula app/detalle/
- calificar pelicula app/detalle/
- desactivar comentario app/detalle/

url principal : 100.25.14.123/app/home

rds : movies1.cbtcodtoytt9.us-east-1.rds.amazonaws.com
port : 5432
user: postgres
pass : movies2020

ec2 : ssh -i "cloud.pem" ubuntu@ec2-100-25-14-123.compute-1.amazonaws.com

